package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import com.example.mysqlRest.model.pax.PaxAddress;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class AddressTypeRepositoryIT {
    @Autowired
    AddressTypeRepository addressTypeRepository;

    @Autowired
    PassengerRepository passengerRepository;

    @Test
    void findByCode() {

        AddressType h = addressTypeRepository.findByCode("H");
        log.info(h.getName());
        assertEquals("H", h.getCode());
    }

    @Test
    @Transactional
    void aaaa() {

        List<Passenger> passengerList = passengerRepository.findAddressTypeCode("H");
        log.info("" + passengerList.size());
//        @Valid List<PaxAddress> addresses = passengerList.get(0).getAddresses();
       // assertEquals(3, addresses.size());
//        assertTrue(addresses.size() > 0);
        //assertEquals("H", passengerList.get(0).getCode());

        List<Passenger> byGender = passengerRepository.findByGender(Gender.F);
        assertTrue(byGender.size() > 0);
    }
}