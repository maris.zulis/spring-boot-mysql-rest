package com.example.mysqlRest.dao;

import com.example.mysqlRest.model.Country;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
@Transactional
public class CountryDaoImpl extends AbstractHibernateDao {

	public List<Country> list() {
		Session session = getSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Country> query = cb.createQuery(Country.class);
		Root<Country> root = query.from(Country.class);
		query.select(root).orderBy(cb.asc(root.get("code")));

		return session.createQuery(query).setCacheable(true).getResultList();
	}

	public Country findByCodeFromCache(String code) {
		List<Country> list = list();
		for (Country country : list) {
			if (country.getCode().equals(code)) {
				return country;
			}
		}

		return null;
	}

	public Country findByCode(String code) {
		Session session = getSession();

		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Country> query = cb.createQuery(Country.class);
		Root<Country> root = query.from(Country.class);

		return session.createQuery(
				query.select(root)
						.where(cb.equal(root.get("code"), code))
		).uniqueResult();
	}
}
