package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    @Query("FROM Passenger p " +
            "join p.addresses x WHERE x.type.code=(:addressTypeCode)")
    List<Passenger> findAddressTypeCode(@Param("addressTypeCode") String addressTypeCode);

    @Query("FROM Passenger p WHERE p.gender=(:gender)")
    List<Passenger> findByGender(@Param("gender") Gender gender);
}
