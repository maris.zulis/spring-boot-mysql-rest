CREATE TABLE `pax_visited_countries` (
  `passenger_id` bigint(20) NOT NULL,
  `visited_countries_id` bigint(20) NOT NULL,
  PRIMARY KEY (`passenger_id`,`visited_countries_id`),
  KEY `FK1q3ssxwa2e47wg0u7s6kl2d9y` (`visited_countries_id`),
  CONSTRAINT `FK1q3ssxwa2e47wg0u7s6kl2d9y` FOREIGN KEY (`visited_countries_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `FK6xn9c5est28qw8c2lbw20udyw` FOREIGN KEY (`passenger_id`) REFERENCES `pax` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8